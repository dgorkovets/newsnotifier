#!/usr/bin/python
# -*- coding: utf-8 -*-
import pynotify
import urllib
from lxml import etree, html
import sys
import time
from datetime import date


def sendmessage(title, message, img):
    pynotify.init("Test")
    notice = pynotify.Notification(title, message)
    notice.set_timeout(pynotify.EXPIRES_NEVER)

    notice.show()

def get_all_messages():
    today = date.today().strftime('%Y/%m/%d')
    f = urllib.urlopen('http://chelyabinsk.ru/text/newsline/' + today + '/')
    content = f.read()
    h = etree.HTML(content, parser=etree.HTMLParser(encoding='windows-1251'))
    #tree = etree.parse(content, parser)
    #root = tree.getroot()
    news = h.xpath('//div[@class="news-record"]')
    newsarray = []
    for n in news:
        title = n.xpath('.//span[@class="title2"]')[0].text
        alltext = '\n'.join(x.text for x in n.xpath('.//p') if x.text)
        try:
            img = n.xpath('.//img/@src')[0]
        except:
            img = ''
        newsarray.append({'title' : title, 'body' : alltext, 'img' : str(img)})
    return newsarray

def notify_last():
    last_title = ""
    while True:
        item = get_all_messages()[0]
        if last_title != item['title']:
            sendmessage(item['title'], item['body'], item['img'])
            last_title = item['title']
        time.sleep(30)



if __name__ == '__main__':
    notify_last()
